import "./styles/App.css";
import Nav from "./components/Nav";
import Intro from "./components/Intro";
import Projects from "./components/Projects";
import ScrollTop from "./components/ScrollTop";
import Contact from "./components/Contact";
import { useEffect, useState } from "react";
import Logo from "./components/Logo";
import About from "./components/About";
import Footer from "./components/Footer";

function App() {
  const [show, setShow] = useState(true);
  useEffect(() => {
    
    const time = setTimeout(() => {
      setShow(false);
    }, 3000);
    return () => {clearTimeout(time);};
  }, []);
  const content = (
    <div className="main">
      <Nav />
      <Intro />
      <Projects />
      <ScrollTop />
      <About/>
      <Contact />
      <Footer/>
    </div>
  );
  return (
    <div className={show ? "App-logo" : "App"}>
      <Logo />
      {show || <>{content}</>}
    </div>
  );
}

export default App;
