// import useScroll from "../hook/useScroll";
import "../styles/about.css";
import { anime } from "../tools/animate";
import { desc, desc1 } from "../data/data";
import { useEffect } from "react";
const lg = ["HTML", "CSS", "JS"];
const fr = ["React", "R-TS", "DOM"];
const opt = [
  "redux",
  "mui",
  "axios",
  "grommet",
  "moment",
  "local",
  "slick",
  "icons",
];

const About = () => {
  // const { scrlY } = useScroll();
  // const el = document.getElementById("about");

  useEffect(()=>{anime()},[])

  // let classOption = el && el?.clientHeight < scrlY + 50;
  // if(el && el?.offsetHeight < scrlY - 1200){
  //   classOption = false
    
  // }
  const content = (
    <div
      className={
        "about opacity1"
      }
      id="about"
    >
      <h2>About</h2>
      <div className="about-cont">

      <div className="about-desc">
        <p className="desc">{desc}</p>
        <p className="desc1">{desc1}</p>
      </div>
      <div className="rotated-lang">
        <ul className="lg" id="ul-lg">
          {lg.map((item, i) => {
            return (
              <li className={`li-lg${i}`} key={i}>
                {item}
              </li>
            );
          })}
        </ul>
        <ul className="fr" id="ul-fr">
          {fr.map((item, i) => {
            return (
              <li className={`li-fr${i}`} key={i}>
                {item}
              </li>
            );
          })}
        </ul>
        <ul className="opt" id="ul-opt">
          {opt.map((item, i) => {
            return (
              <li className={`li-opt${i}`} key={i}>
                {item}
              </li>
            );
          })}
        </ul>
      </div>
          </div>
    </div>
  );
  return <>{content}</>;
};

export default About;
