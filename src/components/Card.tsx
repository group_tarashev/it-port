import "../styles/card.css";
import { AiOutlineLink, AiFillGitlab } from "react-icons/ai";
type CardProp = {
  item: {
    image: string;
    name: string;
    tools: Array<string>;
    framework: string;
    description: string;
    original: string;
    git: string;
  };
};

const Card = ({ item }: CardProp) => {
  return (
    <div className="card">
      <img src={item.image} alt="PromptBase" />
      <div className="content">
        <p className="card-name">{item.name}</p>
        <div className="card-desc">
          <p>{item.description}</p>
        </div>
        <div className="links">
          <a href={item.original} target="_blank">
            <AiOutlineLink color={"white"} size={30} />
          </a>
          <a href={item.git} target="_blank">
            <AiFillGitlab color={"white"} size={30} />
          </a>
        </div>
      </div>
    </div>
  );
};

export default Card;
