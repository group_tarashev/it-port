import "../styles/contact.css";
import { AiFillGitlab, AiFillLinkedin } from "react-icons/ai";
import emailjs from "@emailjs/browser";
import { useRef, useState } from "react";

const emjsID = import.meta.env.VITE_APP_JS_ID;
const emjsTMP = import.meta.env.VITE_APP_JS_TMP;
const emjsKey = import.meta.env.VITE_APP_JS_KEY;

const Contact = () => {
  console.log(emjsID, emjsKey, emjsTMP);
  const form = useRef<HTMLFormElement | null>(null);
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [message, setMessage] = useState("");
  const onSubmit = (e: any) => {
    e.preventDefault();

    form.current &&
      emailjs.sendForm(emjsID, emjsTMP, form.current, emjsKey).then(
        (result) => {
          console.log(result.text);
          setEmail("");
          setName(""), setMessage("");
        },
        (error) => {
          console.log(error.text);
        }
      );
  };
  // const el = document.getElementById("contact-1");
  // const { scrlY } = useScroll();

  // const elOption = el && el?.offsetTop - 760 < scrlY;
  return (
    <div id="contact-1" className={"contact opacity1 "}>
      <div className="left-cont">
        <div className="contact-bg"></div>
        <h2 data-content="Contact">Contact</h2>
        <form ref={form} className="form-contact" onSubmit={onSubmit}>
          <input
            type="text"
            placeholder="Name"
            name="user_name"
            id="username"
            required={true}
            onChange={(e) => setName(e.target.value)}
            value={name}
          />
          <input
            type="email"
            placeholder="Email"
            name="user_email"
            id="email"
            required={true}
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
          <textarea
            name="message"
            required={true}
            id="message"
            value={message}
            cols={30}
            rows={10}
            placeholder="Text here..."
            onChange={(e) => setMessage(e.target.value)}
          ></textarea>
          <button className="btn-submit" type="submit">
            Send
          </button>
        </form>
        <div className="contact-img">
          <img src="/img/1327192289.svg" alt="pexel" />
        </div>
      </div>
      <div className="right-cont">
        <a href="https://gitlab.com/group_tarashev" target="_blank">
          <AiFillGitlab />
        </a>
        <a
          href="https://www.linkedin.com/in/iliyan-tarashev-a7063b122"
          target="_blank"
        >
          <AiFillLinkedin />
        </a>
      </div>
    </div>
  );
};

export default Contact;
