import '../styles/about.css'
const Footer = () => {
  const year = new Date().getFullYear();
  return (
    <div
      style={{
        width: "100%",
        height: 50,
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <span className="desc">Copyright &copy; IT {year}</span>
    </div>
  );
};

export default Footer;
