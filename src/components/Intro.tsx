import { useEffect, useState } from "react";
import "../styles/intro.css";
import Progress from "./Progress";
import { orgText, pro } from "../data/data";

const Intro = () => {
  const [intro, setIntro] = useState("");
  const [last, setLast] = useState("");
  const [scrll, setScrll] = useState(window.scrollY)
  const handleScroll = () =>{
    setScrll(window.scrollY)
  }
  useEffect(()=>{
    document.addEventListener('scroll', handleScroll)
    console.log(scrll);
    
    return () => document.removeEventListener('scroll', handleScroll)
  },[])
  
  useEffect(() => {
    let currTextInd = 0;
    const time = setInterval(() => {
      if (currTextInd < orgText.length - 1) {
        setLast(orgText[currTextInd]);
        setIntro((prev) => prev + orgText[currTextInd]);
        currTextInd++;
      } else {
        clearInterval(time);
        setLast("");
      }
    }, 40);
    return () => clearInterval(time);
  }, []);

  return (
    <div className="intro">
      <span className="circle" style={{transform:`translateY(${scrll + 150}px)`}}></span>
      <div className="left">
        <p style={{filter: `blur(${scrll /500}px)`}}>
          <span>W</span>
          {intro} <span className="intro-last">{last}</span>{" "}
        </p>
      </div>
      <div className="right">
        <div className="progress-r">
          {pro.map((item, i) => (
            <Progress key={i} width={item.width} name={item.lang} />
          ))}
        </div>
      </div>
      <div className="img"  style={{transform:`rotate(${scrll / 12}deg)`}}>
        <img src="/img/1327192289.svg" alt="pexel"  />
      </div>
      <div className="img1" style={{transform:`rotate(${scrll / 12}deg)`}}>
        <img src="/img/1327192289.svg" alt="pexel" />
      </div>
    </div>
  );
};

export default Intro;
