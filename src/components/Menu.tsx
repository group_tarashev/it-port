import { MenuBottonProps } from "./MenuButton";
import { scrollInto } from "../tools/scroll";
import "../styles/menu.css";
const Menu = ({ click, setClick }: MenuBottonProps) => {
  return (
    <div className={click ? "menu" : "close-menu"}>
      <ul className={click ? "menu-list" : "menu-list-close"}>
        <li
          aria-details="P"
          onClick={() => {
            scrollInto("project");
            scrollInto('')
            setClick(false);
          }}
          className="li-item"
        >
          Projects
        </li>
        <li
          aria-details="A"
          onClick={() => {
            scrollInto("about");
            setClick(false);
          }}
          className="li-item"
        >
          About
        </li>
        <li
          aria-details="C"
          onClick={() => {
            scrollInto("contact-1");
            setClick(false);
          }}
          className="li-item"
        >
          Contact
        </li>
      </ul>
    </div>
  );
};

export default Menu;
