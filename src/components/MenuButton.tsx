import '../styles/menu.css'

export type MenuBottonProps ={
    click: boolean,
    setClick: (click: boolean) => void;
}
const MenuButton = ({click, setClick}: MenuBottonProps) => {
  return (
    <div className={click ? "btn-close" :"btn-menu"} onClick={() => setClick(!click)}>
            <span></span>
            <span></span>
            <span></span>
        </div>
  )
}

export default MenuButton