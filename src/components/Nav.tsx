import { useState } from 'react'
import '../styles/nav.css'
import MenuButton from './MenuButton'
import Menu from './Menu'
const Nav = () => {
    const [click,setClick] = useState<boolean>(false)
  return (
    <div className='nav' id='nav'>
        {/* <Logo/> */}
        <MenuButton click={click} setClick={setClick}/>
        <Menu click={click} setClick={setClick}/>
    </div>
  )
}

export default Nav