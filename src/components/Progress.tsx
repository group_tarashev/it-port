import '../styles/progress.css'

type ProgProps = {
  width: number;
  name: string;
};

const Progress = ({ width, name }: ProgProps) => {
  return (
    <div className="progress">
      <span className='pro-name' >{name}</span>
      <div className="bar">
        <div className="percent">
            <span>0</span>
            <span>%</span>
            <span>100</span>
        </div>
        <span className="full-bar"></span>
        <span className="pro-bar" style={{ width: `${width}%` }}></span>
      </div>
    </div>
  );
};

export default Progress;
