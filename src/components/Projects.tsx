import "../styles/projects.css";
import { data } from "../data/data";
import Card from "./Card";
import "keen-slider/keen-slider.min.css";
import "@splidejs/react-splide/css";
// import useScroll from "../hook/useScroll";
import "react-perfect-scrollbar/dist/css/styles.css";


const Projects = () => {
  // const elm = window.document.getElementById("project");
  // const { scrlY } = useScroll();
  // const {x} = useSize();
  // let elmOption = elm && elm?.clientHeight + 1000 < scrlY;
  // if (elm && elm?.offsetHeight < scrlY - 100) {
  //   elmOption = false;
  // }

  const onTouch = ( offset: number) => {
    const elm = document.getElementsByClassName("project-lg")[0];
    const proElm = document.getElementsByClassName("project-content-lg");

    const total = Array.from(proElm).reduce(
      (acc, element) => acc + (element as HTMLElement).clientWidth,
      0
    );

    let currScroll = elm.scrollLeft;

    const nextScroll = currScroll + offset;

    if (nextScroll <= total + offset) {
      elm.scrollTo({
        left: nextScroll,
        behavior: "smooth",
      });
    } else {
      elm.scrollTo({
        left: 0,
        behavior: "smooth",
      });
    }
  };

  

  const lgContent = (
    <div className="project-container" id="project">
      <h2>Caple Personal Projects</h2>
      <div className="project-lg">
        <div className="project-content-lg">
          {/* <HScrollView className={"project-lg"}> */}
          {data.map((item, i) => (
            <Card item={item} key={i} />
          ))}
          {/* </HScrollView> */}
        </div>
      </div>
      <div className="button-group">
        <button onClick={() => onTouch(-620)} className="btn-prev">
          <span>B</span>
          <span>A</span>
          <span>C</span>
          <span>K</span>
        </button>
        <button onClick={() => onTouch(620)} className="btn-next">
          <span>N</span>
          <span>E</span>
          <span>X</span>
          <span>T</span>
        </button>
      </div>
    </div>
  );

  return <>{ lgContent}</>;
};

export default Projects;
