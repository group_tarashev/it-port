import { scrollInto } from "../tools/scroll";
import { MdOutlineKeyboardDoubleArrowUp } from "react-icons/md";
import "../styles/buttons.css";
import { useEffect, useState } from "react";
const ScrollTop = () => {
  const [show, setShow] = useState<boolean>(false);
  const handleShow =() =>{
      if (window.scrollY > 50) {
        setShow(true);
      } else {
        setShow(false);
      }
  }
  useEffect(() => {
    window.addEventListener("scroll",handleShow);
    return () => window.removeEventListener("scroll", handleShow);
  }, [window.scrollY]);
  return (
    <>
      {show && (
        <button
          className={`${show && "btn-top btn-top-show"}`}
          onClick={() => scrollInto('nav')}
        >
          <span></span>
          <MdOutlineKeyboardDoubleArrowUp />
        </button>
      )}
    </>
  );
};

export default ScrollTop;
