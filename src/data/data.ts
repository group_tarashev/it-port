export const data = [
    {
        image: 'img/remote.jpg',
        name: 'GitRemote',
        tools: ['TypeScript', "CSS", "RestAPI"],
        framework: 'React-TypeScript',
        description: 'Chalange to build Remote Site. I used Context, axios, pagination, react-roouter-dom',
        original: 'https://githubremote.netlify.app/',
        git: 'https://gitlab.com/group_tarashev/gitremotejob.git'
    },
    {
        image: 'img/weather.jpg',
        name: 'Thunder',
        tools: ['TypeScript', "CSS", "RestAPI"],
        framework: 'React-TypeScript',
        description: 'Chalange to build Weather with search engine API',
        original: 'https://thunder-app-api.netlify.app',
        git: 'https://gitlab.com/group_tarashev/thunder.git'
    },
    {
        image: 'img/prompt.jpg',
        name: 'Prompt Base',
        tools: ['JavaScript', "CSS"],
        framework: 'React',
        description: 'Copy of promptbase. React-slick slides ',
        original: 'https://demo-prompts.netlify.app',
        git: 'https://gitlab.com/group_tarashev/prompt.git'
    },
    {
        image: 'img/youtube.jpg',
        name: 'YouTube',
        tools: ['JavaScript', "Material UI", 'Redux'],
        framework: 'React',
        description: 'Copy of youtube. I used Redux toolkit to manipulate the states, design is Material UI with change theme provider. RestAPI is from RapidAPI with search engine',
        original: 'https://yt-democopy.netlify.app',
        git: 'https://gitlab.com/group_tarashev/youtube.git'
    },
    {
        image: 'img/upwork.jpg',
        name: 'UpWork',
        tools: ['JavaScript', 'CSS'],
        framework: 'React',
        description: 'Demo copy of upwork freelancer platform. ',
        original: 'https://demo-upwork.netlify.app/',
        git: 'https://github.com/iliyan90/copy-upwork.git',
    },

];

export const orgText = `Welcome to my portfolio, I am junior front-end developer eager to contribute to innovative and user-friendly web experiences. With a strong foundation in HTML, CSS, and JavaScript, I am excited about the dynamic world of web development and always ready to embrace new technologies. My goal is to contribute meaningfully to projects, collaborate with talented teams, and continue learning and growing in this ever-evolving field. I am excited about the prospect of creating elegant solutions that enhance user experiences.`;
 export const pro = [
    {
      lang: "HTML",
      width: 50,
    },
    {
      lang: "CSS",
      width: 40,
    },
    {
      lang: "JaveScript",
      width: 30,
    },
    {
      lang: "React",
      width: 40,
    },
    {
      lang: "TypeScript",
      width: 20,
    },
  ];

  export const desc =
  "I'm dedicated junior front-end developer with a strong desire to grow and excel in the world of web development. I have a keen interest in crafting beautiful and user-friendly websites and applications.";
export const desc1 =
  "My skills include proficiency in HTML, CSS, and JavaScript, and I am continually expanding my knowledge by exploring modern web technologies and frameworks. I take pride in writing clean and maintainable code to create seamless user experiences.";