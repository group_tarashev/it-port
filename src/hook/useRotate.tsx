import { useEffect, useState } from "react";

const useRotate = (element: string) => {
  const [object, setObject] = useState<NodeListOf<Element> | null>();
  // const arr = pickEl.object.childNodes;
  useEffect(() => {
    const time = setTimeout(() => {
      const pickEl = document.querySelectorAll(element);
      setObject(pickEl);
    }, 1000);
    return () => clearTimeout(time);
  }, []);
  return { object };
};

export default useRotate;
