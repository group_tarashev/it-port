import { useEffect, useState } from 'react'

const useScroll = () => {
    const [scrlY, setScrlY] = useState(window.scrollY)
    useEffect(()=>{
        const getScrl = () =>{
            // console.log(window.scrollY);
            setScrlY(window.scrollY)
          }
           window.addEventListener('scroll',getScrl)
           return () =>  window.removeEventListener('scroll', getScrl)
    },[])
  return {scrlY}
}

export default useScroll