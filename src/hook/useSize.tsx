import { useEffect, useState } from "react";

const useSize = () => {
  const [size, setSize] = useState({
    x: 0,
    y: 0,
    
  });
  useEffect(() => {
    const getWidth = () => {
      setSize({
        x: window.innerWidth,
        y: window.innerHeight,
      });
    };
    document.addEventListener("resize", getWidth);
    getWidth();
    return () => {
      removeEventListener("resize", getWidth);
    }
  }, [size]);
  return size;
};

export default useSize;
