export const anime = () => {
  const start = ["0", "45", "90", "135", "180", "225", "270", "315"];
  const end = ["360", "405", "450", "495", "540", "585", "630", "675"];
  const newTiming = {
    duration: 100000,
    iterations: Infinity,
  };
  const time = setTimeout(() => {
    for (let i = 0; i < 8; i++) {
      let opt = document.getElementsByClassName(`li-opt${i}`)[0] as HTMLElement; // give type to opt
      
      opt.style.transform = `rotate(${
        45 * (i + 1)
      }deg) translate(140px) rotate(-${45 * (i + 1)}deg)`;
      opt.style.position = "absolute";
      opt.animate(
        [
          {
            transform: `rotate(${start[i]}deg) translate(140px) rotate(-${start[i]}deg)`,
          },
          {
            transform: `rotate(${end[i]}deg) translate(140px) rotate(-${end[i]}deg)`,
          },
        ],
        newTiming
      );
    }
  }, 500);
  return () => clearTimeout(time);
};
