export const scrollInto = (id: string) =>{
    const item = document.getElementById(id);
    if(item){
        item?.scrollIntoView({behavior:'smooth', block: 'center'});
    }
}

export const scrollTop = () =>{
    const item = document.getElementById('logo');
    if(item){
        item.scrollIntoView({behavior:'smooth', inline:'center', block:'start'});
    }else{
        console.log('No such ID');
        
    }

}